
*** setting ***
Library    SeleniumLibrary 

Resource  ../resource/Login.robot 

*** Variables***
${Ecran_Name}         New_Ecran_is_ABC
${URL}  http://cd.blueway.fr:20321/BWDesignerFaces/login.jsf 
${Credentials}   Nam  Son  Cyrille 
&{Login_by_id_Pass}     Username=admin    Password=admin 


${Screen_Designer}   //div[@id='tabscontent:tabView:contextWindow_1_content']
${Form}              xpath=//fieldset[@class="ui-fieldset ui-widget ui-widget-content ui-corner-all ui-hidden-container menuCategoryForm"]  
${Block}             xpath=//label[text()='Block' and @class='ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle']

${Form/Block}        css=div[alt='0'] div[alt='0']
${Text}              xpath=//label[text()="Texte" and @class="ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle"]
${Calendar}          //label[text()="Calendrier" and @class="ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle"]
${Edit_text}         //label[text()="Edit_Text" and @class="ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle"]   

${Autocomplete}      //label[text()="AutoComplete" and @class="ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle"]


${Button}            //label[text()="Bouton" and @class="ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle"]
${Uploadfile}        //label[text()='Upload de fichier' and @class='ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle'] 



${Radiobutton}       //label[text()='Bouton radio' and @class='ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle'] 
${Checkbox}          //label[text()='Case à cocher' and @class='ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle'] 


${value1}=    Decode Bytes To String    ${Combobox}

${Combobox}          //label[text()='Liste déroulante' and @class='ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle'] 
${Image}             //label[text()='Image' and @class='ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle'] 


${Hyperlink}         //label[text()='Hyperlien' and @class='ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle'] 
${Map}               //label[text()='Carte' and @class='ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle']  

${Scheduler}         //label[text()='Planificateur' and @class='ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle'] 
${Siderbar}          //label[text()='SideBar' and @class='ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle']   



*** Test Cases ***
  

LoginPageBlueway
    [Tags]    Login
    [Documentation]    Login Page 
    Open Browser    http://cd.blueway.fr:20321/BWDesignerFaces/login.jsf    Chrome     
    Click Element    id=loginForm:username    
    Input Text    id=loginForm:username    admin    
    Click Element    id=loginForm:password    
    Input Password    id=loginForm:password    admin     
    #Input Password    id=loginForm:password    &{Login_by_id_Pass}[Password]   
    Click Element    xpath=//button/span    
    Maximize Browser Window
    


Designer_New_Ecran
    [Tags]   Create a new Ecran 
    Wait Until Page Contains Element    id=rigthmenu:idTopMenuItem:7:menuButton     
    Click Element    id=rigthmenu:idTopMenuItem:7:menuButton     
    Sleep    3        
    Click Element    xpath=//span[(text() = 'Ecran' or . = 'Ecran')]       
    Sleep    2      
    Click Element        xpath=(//LABEL[@title=''][text()='Nouveau'][text()='Nouveau'])[33]      
    Sleep    2   
    Wait Until Page Contains Element     id=tabscontent:tabView:edittext_0_0     
    Click Element    id=tabscontent:tabView:edittext_0_0    
    Input Text    id=tabscontent:tabView:edittext_0_0    component_utf8_23
    Click Element    css=.fa-floppy-o
    
Click Editor
    Sleep    2     
    Click Element    xpath=//a[@href="#" and @class="ui-commandlink ui-widget" and text()='Editeur']
     

Drag drop Block
    Sleep    2 
    #Mouse Over    xpath=//legend[text()='Form' and @class='ui-fieldset-legend ui-corner-all ui-state-default']   
    #Wait Until Element Is Visible  xpath=//legend[text()='Form' and @class='ui-fieldset-legend ui-corner-all ui-state-default']
    
    #Click Element    tabscontent:tabView:j_idt1203     // Element : Is OK 
    Click Element    xpath=//fieldset[@class="ui-fieldset ui-widget ui-widget-content ui-corner-all ui-hidden-container menuCategoryForm"]              
    
    #Click Element    xpath=//legend[text()='Form' and @class='ui-fieldset-legend ui-corner-all ui-state-default']     // Element : Not visible in Blueway 
    Sleep    2    
    #Click Element    xpath=//label[text()='Block' and @class='ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle'] 
    
    Drag And Drop       ${Block}   ${Screen_Designer}
    
Drag drop Combobox
    Sleep    2
    Click Element    ${Form} 
    Drag And Drop   ${Combobox}       ${Form/Block}


 Drag drop Checkbox
    Sleep    3
    Click Element    ${Form} 
    Drag And Drop   ${Checkbox}         ${Form/Block}