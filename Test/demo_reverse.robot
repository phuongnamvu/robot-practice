
*** setting ***
Library    SeleniumLibrary 

Resource  ../resource/Login.robot 

*** Variables***
${Ecran_Name}         New_Ecran_is_ABC
${URL}  http://cd.blueway.fr:20321/BWDesignerFaces/login.jsf 
${Credentials}   Nam  Son  Cyrille 
&{Login_by_id_Pass}     Username=admin    Password=admin 


${Screen_Designer}   //div[@id='tabscontent:tabView:contextWindow_1_content']
${Form}              xpath=//fieldset[@class="ui-fieldset ui-widget ui-widget-content ui-corner-all ui-hidden-container menuCategoryForm"]  
${Block}             xpath=//label[text()='Block' and @class='ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle']

${Form/Block}        css=div[alt='0'] div[alt='0']
${Text}              xpath=//label[text()="Texte" and @class="ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle"]
${Calendar}          //label[text()="Calendrier" and @class="ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle"]
${Edit_text}         //label[text()="Edit_Text" and @class="ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle"]   

${Autocomplete}      //label[text()="AutoComplete" and @class="ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle"]


${Button}            //label[text()="Bouton" and @class="ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle"]
${Uploadfile}        //label[text()='Upload de fichier' and @class='ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle'] 



${Radiobutton}       //label[text()='Bouton radio' and @class='ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle'] 
${Checkbox}          //label[text()='Case à cocher' and @class='ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle'] 


${value1}=    Decode Bytes To String    ${Combobox}

${Combobox}          //label[text()='Liste déroulante' and @class='ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle'] 
${Image}             //label[text()='Image' and @class='ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle'] 


${Hyperlink}         //label[text()='Hyperlien' and @class='ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle'] 
${Map}               //label[text()='Carte' and @class='ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle']  

${Scheduler}         //label[text()='Planificateur' and @class='ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle'] 
${Siderbar}          //label[text()='SideBar' and @class='ui-outputlabel ui-widget bwcomponentname dragIcon ui-draggable-handle']   



*** Test Cases ***
  

LoginPageBlueway
    [Tags]    Login
    [Documentation]    Login Page 
    Open Browser    http://cd.blueway.fr:20321/BWDesignerFaces/login.jsf    Chrome     
    Sleep    1
    Click Element    id=loginForm:username    
    Sleep    1
    Input Text    id=loginForm:username    admin  
    Sleep    1  
    Click Element    id=loginForm:password    
    Sleep    1
    Input Password    id=loginForm:password    admin     
    Sleep    1
    #Input Password    id=loginForm:password    &{Login_by_id_Pass}[Password]   
    Click Element    xpath=//button/span  
    Sleep    1  
    Maximize Browser Window
    
Designer_New_Physical
    [Tags]   Create a new physical 
    Wait Until Page Contains Element    id=rigthmenu:idTopMenuItem:5:menuButton     
    Click Element    id=rigthmenu:idTopMenuItem:5:menuButton  
    Sleep    1        
    Click Element    xpath=//span[(text() = 'Base de données' or . = 'Base de données')] 
        Sleep    2      
    Click Element        xpath=((.//*[normalize-space(text()) and normalize-space(.)='Base de données'])[2]/following::label[1])
    Sleep    1   
    Wait Until Page Contains Element     xpath=(//div/div/div/div/div/div/div/div/div/div/div/table/tbody/tr/td[2]/input)    
    Click Element    xpath=(//div/div/div/div/div/div/div/div/div/div/div/table/tbody/tr/td[2]/input)    
    Input Text    xpath=(//div/div/div/div/div/div/div/div/div/div/div/table/tbody/tr/td[2]/input)    robot3
    Click Element  xpath=(//div/div/div/div/div/div/div/table/tbody/tr[3]/td[2]/div/div[3]/span)
    Wait Until Page Contains Element  //ul[@id='tabscontent:tabView:supportPhy_combobox_0_2_items']/li[2]
    Sleep    1
    Click Element  xpath=(//ul[@id='tabscontent:tabView:supportPhy_combobox_0_2_items']/li[2])
    Sleep    1
    Click Element  xpath=(//div[2]/table/tbody/tr/td[2]/div/div[3]/span)
    Wait Until Page Contains Element  xpath=(.//*[normalize-space(text()) and normalize-space(.)='}'])[2]/following::li[2]
    Click Element  xpath=(.//*[normalize-space(text()) and normalize-space(.)='}'])[2]/following::li[2]
    Click Element  xpath=(//td[3]/div/div[3]/span)
    Wait Until Page Contains Element  xpath=(//div[75]/div/ul/li[7])
    Click Element  xpath=(//div[75]/div/ul/li[7])
    Sleep    1
    
    Click Element    xpath=(//div[2]/table/tbody/tr/td[4])
    Click Element    xpath=(//td[4]/div/div[2]/input)
    Sleep    1   
    Input Text    xpath=(//td[4]/div/div[2]/input)    root 
    
    Click Element    xpath=(//td[5])
    Click Element    xpath=(//td[5]/input)
    Input Text    xpath=(//td[5]/input)    password 
    
    Click Element    xpath=(//td[6])
    Click Element    xpath=(//td[6]/div/div[2]/input)
    Input Text    xpath=(//td[6]/div/div[2]/input)    test  
    
    Click Element    xpath=(//td[7])
    Click Element    xpath=(//td[7]/div/div[2]/input)
    Input Text    xpath=(//td[7]/div/div[2]/input)    test 
    
    Click Element    xpath=(//td[8])
    Click Element    xpath=(//td[8]/div/div[2]/input)
    Input Text    xpath=(//td[8]/div/div[2]/input)    test 
    
    Click Element     xpath=(//td[9]/div/div[3]/span)
    Sleep    1 
    Click Element     xpath=((.//*[normalize-space(text()) and normalize-space(.)='sybase'])[2]/following::li[2])
    
    Click Element        xpath=( //td[10])
    Click Element        xpath=(//td[10]/div/div[2]/input)
    Input Text          xpath=(//td[10]/div/div[2]/input)   20378
    
    Click Element       xpath=(//div[2]/div/div/div/div[2]/div/div/button/span)
    Sleep    3
    Click Element       xpath=((.//*[normalize-space(text()) and normalize-space(.)='Reverse'])[1]/preceding::span[1])
    Sleep    4
    Click Element      xpath=(//div[3]/div/div/div/div/span)
    Sleep    1
   Click Element      xpath=(//div[3]/div/div/div/div/span)
    Sleep    1
    Click Element      xpath=(//table[2]/tbody/tr/td[2]/button/span[2])
    Sleep    1
    Click Element      xpath=(//tr[2]/td/button/span[2])
    Sleep    1
    Click Element      xpath=(//tr[2]/td[2]/button/span[2])
    Sleep    5
    Close Browser
   
    
    